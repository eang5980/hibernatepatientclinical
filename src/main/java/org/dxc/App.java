package org.dxc;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import org.dxc.entity.ClinicalData;
import org.dxc.entity.Patient;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

/**
 * Hello world!
 *
 */
public class App {
	private static SessionFactory factory;

	public static void main(String[] args) {
		try {
			factory = new Configuration().configure().buildSessionFactory();
		} catch (Throwable e) {
			System.err.println("Failed to create Session Instance");
			throw new ExceptionInInitializerError(e);
		}

		Session session = factory.openSession();
		Transaction tx = session.beginTransaction();

		LocalDateTime currentDatetime = LocalDateTime.now();


		ClinicalData c1 = new ClinicalData();
		c1.setComponentName("Height");
		c1.setComponentValue(1.52);
		c1.setMeasuredDatetime(currentDatetime);

		ClinicalData c2 = new ClinicalData();
		c2.setComponentName("Weight");
		c2.setComponentValue((double) 65);
		c2.setMeasuredDatetime(currentDatetime);

		ArrayList<ClinicalData> list1 = new ArrayList<ClinicalData>();
		list1.add(c1);
		list1.add(c2);

		ClinicalData c3 = new ClinicalData();
		c1.setComponentName("Height");
		c1.setComponentValue(1.80);
		c1.setMeasuredDatetime(currentDatetime);

		ClinicalData c4 = new ClinicalData();
		c2.setComponentName("Weight");
		c2.setComponentValue((double) 62);
		c2.setMeasuredDatetime(currentDatetime);
		
		ArrayList<ClinicalData> list2 = new ArrayList<ClinicalData>();
		list1.add(c3);
		list1.add(c4);

		Patient p1 = new Patient();
		p1.setFirstName("Ed");
		p1.setLastName("Ang");
		p1.setAge(28);
		p1.setClinicalData(list1);

		Patient p2 = new Patient();
		p2.setFirstName("John");
		p2.setLastName("Tan");
		p2.setAge(28);
		p2.setClinicalData(list2);

		
		

		session.persist(p1);
		session.persist(p2);

		tx.commit();

		session.close();
		System.out.println("success");

	}

}
