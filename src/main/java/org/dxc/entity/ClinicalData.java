package org.dxc.entity;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Table(name="clinicaldata")
public class ClinicalData {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;
	
	@NotNull
	private String componentName;
	
	@NotNull
	private double componentValue;
	
	@NotNull
	private LocalDateTime measuredDatetime;

	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getComponentName() {
		return componentName;
	}
	public void setComponentName(String componentName) {
		this.componentName = componentName;
	}
	public Double getComponentValue() {
		return componentValue;
	}
	public void setComponentValue(Double componentValue) {
		this.componentValue = componentValue;
	}
	public LocalDateTime getMeasuredDatetime() {
		return measuredDatetime;
	}
	public void setMeasuredDatetime(LocalDateTime measuredDatetime) {
		this.measuredDatetime = measuredDatetime;
	}


	


	
	
	
	
	
}
